annotations-numpy
=================

This package defines several validators using the PEP593 Annotated annotation to
verify numpy ndarrays at runtime. To use the validators, annotate function
parameters using Annotated instances and decorate the function with the
npvalidate decorator.

Example
-------

.. code-block:: python

    from typing import Annotated

    from annotations_numpy import npvalidate, Shape
    from numpy import ndarray

    Matrix = Annotated[ndarray, Shape(Shape.ANY, Shape.ANY, match_ndims=True)]

    @npvalidate
    def frobnicate(mat: Matrix) -> int:
        """mat is guaranteed to have 2 axes in this function.

        If a value for mat is provided that does not match the shape, a
        ValueError will be raised
        """
        ...

    

