from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Sequence

from numpy import ndarray


class NumpyValidator(ABC):
    @abstractmethod
    def validate(self, value: ndarray) -> None:
        raise NotImplementedError()


class DimensionSpec(ABC):
    @abstractmethod
    def check(self, value: int) -> None:
        raise NotImplementedError()


class Fixed(DimensionSpec):
    def __init__(self, target: int):
        self.target = target

    def check(self, value: int) -> None:
        if value != self.target:
            raise ValueError(f"dimension mismatch: got {value}, expected: {self.target}")


class Interval(DimensionSpec):
    def __init__(self, lower: int, upper: int, inclusive: bool = True):
        self.lower = lower
        self.upper = upper
        self.inclusive = inclusive

    def check(self, value: int) -> None:
        if self.inclusive and not self.lower <= value <= self.upper:
            raise ValueError(
                f"dimension mismatch: got {value}, expected value in interval [{self.lower}, {self.upper}]"
            )
        elif not self.lower < value < self.upper:
            raise ValueError(
                f"dimension mismatch: got {value}, expected value in interval ({self.lower}, {self.upper})"
            )


class Ignored(DimensionSpec):
    def check(self, value: int) -> None:
        return


class Shape(NumpyValidator):
    def __init__(self, spec: Sequence[DimensionSpec], match_length: bool = False):
        self.shape_spec = spec
        self.match_length = match_length

    def validate(self, value: ndarray) -> None:
        spec_len = len(self.shape_spec)
        val_len = len(value.shape)

        if self.match_length and spec_len != val_len:
            raise ValueError("Shape lengths do not match")
        elif spec_len > val_len:
            raise ValueError(f"Missing {spec_len - val_len} dimensions")

        for i, dim_spec in enumerate(self.shape_spec):
            dim_spec.check(value.shape[i])
