from __future__ import annotations

from abc import ABC, abstractmethod
from typing import Tuple

from numpy import ndarray, dtype


class NumpyConverter(ABC):
    @abstractmethod
    def convert(self, value: ndarray) -> ndarray:
        raise NotImplementedError()


class Dtype(NumpyConverter):
    def __init__(self, dtype_: dtype):
        self.dtype = dtype_

    def convert(self, value: ndarray) -> ndarray:
        return value.astype(self.dtype)
