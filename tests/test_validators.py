from annotations_numpy.validators import Shape
from numpy import array
from pytest import raises


def test_shape():
    validator = Shape((1, 2))
    test_val = array([[10, 20]])

    validator.validate(test_val)

    test_val2 = array([10, 2])

    with raises(ValueError):
        validator.validate(test_val2)


def test_shape_any():
    validator = Shape((Shape.ANY, 2))
    test_val1 = array([[1, 2]])
    test_val2 = array([[1, 2], [3, 4]])

    validator.validate(test_val1)
    validator.validate(test_val2)

    test_val3 = array([1, 2])
    test_val4 = array([[1, 2, 3]])

    with raises(ValueError):
        validator.validate(test_val3)
        validator.validate(test_val4)
