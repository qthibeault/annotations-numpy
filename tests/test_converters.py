from annotations_numpy.converters import Dtype
from numpy import array, float64, float32

def test_dtype():
    converter = Dtype(float64)
    test_val = array([1, 2, 3], dtype=float32)
    conv_val = converter.convert(test_val)

    assert conv_val.dtype == float64
